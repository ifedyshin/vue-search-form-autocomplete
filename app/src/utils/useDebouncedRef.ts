import { ref, customRef } from 'vue'

const debounce = (cb: (value: string) => void, delay = 0, immediate = false) => {
  let timeout: number;
  return (value: string) => {
    if (immediate && !timeout) {
      cb(value);
    }
    clearTimeout(timeout);

    timeout = setTimeout(() => {
      cb(value);
    }, delay);
  }
}

const useDebouncedRef = (initValue: string, delay: number, immediate = false) => {
  const state = ref(initValue);
  const debouncedRef = customRef((track, trigger) => ({
    get () {
      track();
      return state.value;
    },
    set: debounce(
      (value: string) => {
        state.value = value;
        trigger();
      },
      delay,
      immediate
    ),
  }));
  return debouncedRef;
}

export default useDebouncedRef;
