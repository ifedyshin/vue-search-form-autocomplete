## Getting Started

### Copy env file

```bash
cp .env.example .env
```

### Serve using local Nodejs
```bash
yarn install && yarn serve
```
### Or using Docker container
```bash
make npmi && make upd
```

```bash
# to view logs
make log

# to stop container
make down
```

App will running at: http://localhost:8080/
