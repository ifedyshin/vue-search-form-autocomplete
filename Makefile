include ./.env
.PHONY: all

SHELL=/bin/bash
NODE_IMAGE=node:$(NODE_IMAGE_VERSION)
CONTAINER_USER=node
DOCKER_RUN_NODE=docker run -it \
	--rm \
	-v ${PWD}:$(CONTAINER_WORKDIR) \
	-w="$(CONTAINER_WORKDIR)" \
	-u "$(CONTAINER_USER)"

all: help

help: ## Display this help screen.
	@grep -h -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

npm: ## Up nodejs container.
	$(DOCKER_RUN_NODE) \
	$(NODE_IMAGE) \
	$(SHELL)

npmi: ## Installing dependencies.
	$(DOCKER_RUN_NODE) \
	$(NODE_IMAGE) \
	$(SHELL) -c "cd app && yarn install"

_=

up: ## Run project containers.
	docker-compose up $(_)

upd: ## Run project containers in demon mode.
	docker-compose up -d $(_)

restart: ## Restart project containers.
	docker-compose restart $(_)

log: ## Log app container.
	docker logs -f $(PROJECT_NAME)

down: ## Stop project containers.
	docker-compose down $(_)
